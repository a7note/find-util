set(TEST_TARGET_NAME finder_gtest)

find_package(GTest REQUIRED)

set(TEST_SOURCES
  gtest_main.cpp
  TestFileControllerImpl.cpp
)

add_executable(${TEST_TARGET_NAME}
    ${TEST_SOURCES}
    ../src/FileCollectorImpl.cpp)

target_link_libraries(${TEST_TARGET_NAME}
  PRIVATE GTest::GTest
)

target_include_directories(${TEST_TARGET_NAME}
  PRIVATE ${CMAKE_SOURCE_DIR}/src
)

enable_testing()



