#include <gtest/gtest.h>

#include "FileCollectorImpl.hpp"

#include <filesystem>
#include <fstream>

namespace test
{
namespace
{
namespace fs = std::filesystem;
}

TEST(FileCollectorImplTest, ForRightPathReturnAllFiles)
{
    fs::current_path(fs::temp_directory_path());
    if (fs::exists("sandboxCollectorTest")) {
        fs::remove_all("sandboxCollectorTest");
    }
    fs::create_directories("sandboxCollectorTest/a/b");
    std::ofstream("sandboxCollectorTest/file1.txt");
    fs::create_symlink("a", "sandboxCollectorTest/syma");

    std::stringstream buffer;
    std::streambuf*   previouscout = std::cout.rdbuf(buffer.rdbuf());

    core::FileCollectorImpl collector;
    collector.getFiles("sandboxCollectorTest");

    std::string const text = buffer.str();
    std::string const expected{"./file1.txt\n./syma\n./a\n./a/b\n"};
    EXPECT_EQ(text, expected);

    std::cout.rdbuf(previouscout);
    fs::remove_all("sandboxCollectorTest");
}


TEST(FileCollectorImplTest, ForWrongPathReturnWrongMessage)
{
    fs::current_path(fs::temp_directory_path());
    if (fs::exists("sandboxCollectorThrow")) {
        fs::remove_all("sandboxCollectorThrow");
    }

    std::stringstream buffer;
    std::streambuf*   previouscout = std::cout.rdbuf(buffer.rdbuf());

    core::FileCollectorImpl collector;
    collector.getFiles("sandboxCollectorThrow");

    std::string const text = buffer.str();
    std::string const expected{"Wrong file path!"};
    EXPECT_EQ(text, expected);

    std::cout.rdbuf(previouscout);
}

}  // namespace test
