#pragma once

#include "FileCollector.hpp"

namespace core
{
class FileCollectorImpl : public FileCollector
{
public:
    FileCollectorImpl()           = default;
    ~FileCollectorImpl() override = default;

    void getFiles(std::string const& pathToDirectory) override;
};

}  // namespace core
