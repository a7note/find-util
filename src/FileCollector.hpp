#pragma once

#include <string>

namespace core
{
class FileCollector
{
public:
    virtual ~FileCollector() = default;

    ///@brief Prints paths to all found files
    virtual void getFiles(std::string const& pathToDirectory) = 0;
};

}  // namespace core
