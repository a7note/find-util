#include "FileCollectorImpl.hpp"

#include <dirent.h>

#include <cstring>
#include <iostream>
#include <queue>

namespace core
{
namespace
{
class Directory
{
public:
    explicit Directory(std::string const& path)
      : mDirectory{opendir(path.data())}
    {
        if (mDirectory == nullptr) {
            throw std::invalid_argument("Wrong file path!");
        };
    }

    dirent*
    getEntry()
    {
        return readdir(mDirectory);
    }

    ~Directory()
    {
        closedir(mDirectory);
    }

private:
    DIR* mDirectory{nullptr};
};

bool
isDirectory(dirent* entry)
{
    return (entry->d_type == DT_DIR);
}
bool
isRoot(dirent* entry)
{
    return (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0);
}
}  // namespace

void
FileCollectorImpl::getFiles(std::string const& pathToDirectory)
{
    std::queue<std::string> directories;
    directories.push(pathToDirectory);

    while (!directories.empty()) {
        try {
            Directory dir(directories.front());
            auto      path{std::string(directories.front()).replace(0, pathToDirectory.size(), ".")};

            if (directories.front() != pathToDirectory) {
                std::cout << path << "\n";
            }

            dirent* entry{nullptr};
            while ((entry = dir.getEntry()) != nullptr) {
                if (isDirectory(entry) && !isRoot(entry)) {
                    directories.push(directories.front() + "/" + entry->d_name);
                    continue;
                }
                if (!isRoot(entry)) {
                    std::cout << path + "/" + entry->d_name << "\n";
                }
            };

            directories.pop();
        }
        catch (std::exception& e) {
            std::cout << e.what();
            directories.pop();
        }
    }
}


}  // namespace core
