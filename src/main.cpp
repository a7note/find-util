#include <iostream>
#include <string>

#include "FileCollectorImpl.hpp"

namespace
{
std::string
parseArgument(int argumentsNumber, char** arguments)
{
    if (argumentsNumber == 1) {
        return {};
    }

    return {arguments[1]};
}

}  // namespace

int
main(int argc, char** argv)
{
    auto const path = parseArgument(argc, argv);

    if (path.empty()) {
        std::cout << "Error: the search directory is not specified\n";
        return 1;
    }

    core::FileCollectorImpl parser;
    parser.getFiles(path);

    return 0;
}
